#!/bin/bash
source "$HOME/.dots/install/core.cfg"

# install homebrew if it is not yet installed
if ! hash brew 2>/dev/null; then
  ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"
fi

# install Homebrew managed dependencies
brew bundle --file=$HOME/.dots/Brewfile

# install Rubygems
gem install bundler
bundle install --gemfile=$HOME/.dots/Gemfile
rm -f $HOME/.dots/Gemfile.lock
