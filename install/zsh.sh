#!/bin/bash
source "$HOME/.dots/install/core.cfg"

# delete /etc/zprofile - added by os x 10.11
# path_helper conflicts - http://www.zsh.org/mla/users/2015/msg00727.html
sudo rm -f /etc/zprofile

# install oh-my-zsh
sudo rm -rf $HOME/.oh-my-zsh
gitsync robbyrussell/oh-my-zsh $HOME/.oh-my-zsh
